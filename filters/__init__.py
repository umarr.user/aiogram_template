from loader import dp

from .admin_filters import IsAdmin


if __name__ == 'filters':
    dp.filters_factory.bind(IsAdmin)
