from aiogram import types
from aiogram.dispatcher.filters import BoundFilter
from loader import config


class IsAdmin(BoundFilter):

    async def check(self, *args) -> bool:
        if isinstance(args[0], types.Message):
            return args[0].from_user.id in config.bot.admins
        return False
