from aiogram import types
from filters import IsAdmin
from loader import bot, dp


@dp.message_handler(IsAdmin(), commands=['stop'])
async def on_stop(message: types.Message):
    await message.answer('Bot stopped')
    dp.stop_polling()
