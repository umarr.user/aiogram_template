from aiogram import types
from loader import bot, dp
from utils.database.models import Users


@dp.message_handler(commands=['start'])
async def on_start(message: types.Message):
    await bot.send_message(chat_id=message.from_user.id, text='Привет, пользователь')
