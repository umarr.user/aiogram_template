from aiogram import Bot, Dispatcher, executor, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from data.config import load_config
from utils.database.base import Base
from utils.functions import notify_admins_on_startup, set_bot_commands

config = load_config()

bot = Bot(config.bot.token, parse_mode=types.ParseMode.HTML)
dp = Dispatcher(bot, storage=MemoryStorage())


async def startup(dispatcher: Dispatcher):
    engine = create_async_engine(
        f'postgresql+asyncpg://{config.database.user}:{config.database.password}@{config.database.host}/{config.database.database_name}',
        future=True)

    async with engine.begin() as connection:
        await connection.run_sync(Base.metadata.create_all)

    async_session = sessionmaker(
        engine, expire_on_commit=False, class_=AsyncSession
    )
    dispatcher.bot['db'] = async_session

    await notify_admins_on_startup(dispatcher, config.bot.admins)
    await set_bot_commands(dispatcher)


async def shutdown(dispatcher: Dispatcher):
    await dp.storage.close()
    await dp.storage.wait_closed()
    await bot.session.close()


def load() -> Dispatcher:
    import filters
    import middlewares
    from handlers import dp

    return dp


if __name__ == '__main__':
    executor.start_polling(load(), on_startup=startup,
                           on_shutdown=shutdown, skip_updates=True)
