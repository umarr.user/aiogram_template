from dataclasses import dataclass

from environs import Env


@dataclass
class Bot:
    token: str
    admins: list


@dataclass
class DataBase:
    host: str
    database_name: str
    user: str
    password: str


@dataclass
class Config:
    bot: Bot
    database: DataBase


def load_config():
    env = Env()
    env.read_env()

    return Config(
        bot=Bot(
            token=env.str('TOKEN'),
            admins=[
                env.int('ADMIN_ID')
            ]
        ),
        database=DataBase(
            host=env.str('PG_HOST'),
            database_name=env.str('PG_DATABASE'),
            user=env.str('PG_USER'),
            password=env.str("PG_PASSWORD")
        )
    )
