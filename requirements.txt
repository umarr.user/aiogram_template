aiogram ~=2.13;
asyncpg ~= 0.23.0;
environs ~= 9.3.2;
sqlalchemy ~= 1.4.17;

# Для удобства разработки
sqlalchemy-stubs ~= 0.4;
sqlalchemy2-stubs ~= 0.0.2a2
