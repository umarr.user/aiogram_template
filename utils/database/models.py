from sqlalchemy import BigInteger, Column, Integer, String

from utils.database.base import Base


class Users(Base):
    __tablename__ = "users"

    user_id = Column(BigInteger, primary_key=True, unique=True)
    username = Column(String(32))
