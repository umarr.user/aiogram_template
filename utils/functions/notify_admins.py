from asyncio import sleep
from typing import List

from aiogram import Dispatcher


async def notify_admins_on_startup(dispatcher: Dispatcher, admins: List[int], interval: float = 0.1):
    text = 'Бот запущен'
    for admin in admins:
        try:
            await dispatcher.bot.send_message(admin, text)
            await sleep(interval)
        except:
            pass
