from aiogram import Dispatcher, types


async def set_bot_commands(dispatcher: Dispatcher):
    commands = [
        types.BotCommand('start', 'Запустить бота'),
        types.BotCommand('help', 'Помощь')
    ]
    await dispatcher.bot.set_my_commands(commands)
