from loader import dp

from .chat_ignore import IgnoreGroups


if __name__ == 'middlewares':
    dp.setup_middleware(IgnoreGroups())
