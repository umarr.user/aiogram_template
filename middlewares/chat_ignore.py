from aiogram import types
from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram.dispatcher.handler import CancelHandler, SkipHandler


class IgnoreGroups(BaseMiddleware):

    async def on_pre_process_update(self, update: types.Update, *args):
        if update.message:
            if update.message.chat.type in (types.ChatType.GROUP, types.ChatType.SUPERGROUP):
                raise CancelHandler
